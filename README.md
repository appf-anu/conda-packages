# Conda Packages

Some conda pkgs. See https://anaconda.org/appfanu

```bash
conda config --add channels defaults
conda config --add channels bioconda
conda config --add channels conda-forge
conda config --add channels appfanu
conda install mamba
mamba create -n SOMEENV pkg
```
