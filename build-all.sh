#!/bin/bash

PKGS=("$@")
if [ "${#PKGS[@]}" -lt 1 ]
then
    PKGS+=(conda/*)
fi

set -x

conda build \
        --no-anaconda-upload \
        -c defaults \
        -c conda-forge \
        -c bioconda \
        -c appfanu \
        --skip-existing \
        "${PKGS[@]}"
